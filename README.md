# Beyond Information System
[https://beyond.biosp.inrae.fr/](https://beyond.biosp.inrae.fr/)

| Catalog Demo                               | Text Mining Demo                         | JupyterHub Demo                            |
| ------------------------------------------ | --------------------------------------- | ------------------------------------------ |
| ![Catalog Demo](readme.img/beyond_si_demo.gif)        | ![Text Mining Demo](readme.img/beyond_elk_demo.gif) | ![JupyterHub Demo](readme.img/beyond_remote_sensing_jhub.gif) |


## Aim
Deploy the whole infrastructure of Beyond task 2.1:

+ B-front: Frontweb (and reverse proxy)
+ B-geonetwork: metadata catalog
+ B-ELK: full stack of Elasticsearch
+ B-Jupyterhub: Notebooks Jupyter
+ B-airflowrunner: airflowrunner 
+ B-scheduler: airflow scheduler

![Beyond_SI_infra_schema](readme.img/Beyond_SI_WEB.drawio.png)

## For contributers: develop ansible playbook
You may want to use [vagrant](https://www.vagrantup.com/) to create on your computer the whole Beyond infrastructure (i.e. 5 virtual machines)

## License
This code is provided under the [CeCILL-B](https://cecill.info/licences/Licence_CeCILL-B_V1-en.html) free software license agreement.

## How to use this repository
### Requirements
+ Ansible version > 2.10 with ansible-galaxy
+ Install ansible-galaxy collections used by this repository: `ansible-galaxy install -r ./ansible/requirements.yml`
### On my own computer -> using vagrant and virtual machines (VM) hosted on my computer
+ Start all the VM (be aware it will start 5 VM on your computer)
```
cd vagrant
vagrant up
```
+ Start a VM (start elastic & kibana VM (ELK) for example)
```
cd vagrant
vagrant up B-ELK
```
### On Beyond servers -> using ansible
+ Start a VM (start elastic & kibana VM (ELK) for example)
 ```
cd ansible
ansible-playbook install-elk.yml -u [name] --ask-pass --become --ask-become-pass -i inventories/production/hosts.yml
```
+ with:
	+ -u: the username on the beyond server. Replace [name] by your own account
	+ --aks-pass: if you don't use authentication by ssh key, you can prompt your own password
	+ --become: give ansible your sudo permissions
	+ --ask-become-pass: your password to elevate your permissions as sudo
	+ -i: provides the production inventory file 

## Acknowledgement
+ [vagrant](https://www.vagrantup.com/)
+ [ansible](https://docs.ansible.com/ansible/latest/index.html)
+ [ansible community crypto for SSL certificate](https://galaxy.ansible.com/community/crypto)
+ [geonetwork](https://geonetwork-opensource.org/)
+ [elastic](https://elastic.co/)
+ [jupyter](https://jupyter.org/)
+ [airflow](https://airflow.apache.org/)
+ [geonapi](https://github.com/eblondel/geonapi)
+ [geometa](https://github.com/eblondel/geometa)
